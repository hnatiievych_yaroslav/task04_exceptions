package com.hnatiievych.controller;

import com.hnatiievych.model.Service;
import com.hnatiievych.model.User;
import com.hnatiievych.model.UserAgeException;

public class ControllerImpl implements Controller {


    @Override
    public User createUser(String name, int age) {
        User user = new User();
        try {
            user.setUserName(name);
            user.setAge(age);
        } catch (UserAgeException ex){
            ex.printStackTrace();
        }
        return user;
    }

    @Override
    public void printAutoclouse() {

        try (Service service = new Service()){
            service.printSomeAbracadabra();
        } catch (Exception e){
            System.out.println("it's block catch");
        }
    }
}
