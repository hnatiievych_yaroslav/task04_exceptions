package com.hnatiievych.view;

@FunctionalInterface
public interface Printable {
    void print();
}
