package com.hnatiievych.view;

import com.hnatiievych.controller.Controller;
import com.hnatiievych.controller.ControllerImpl;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scan = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - create User");
        menu.put("2", "  2 - test AutoCloseable");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
    }


    private void pressButton1() {
        System.out.println("Enter user name ");
        String userName = scan.nextLine();
        System.out.println("Enter user age");
        int userAge = scan.nextInt();
        System.out.println(controller.createUser(userName,userAge));
    }

    private void pressButton2() {
        controller.printAutoclouse();
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = scan.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}

