package com.hnatiievych.model;

public class Service implements AutoCloseable {

    public void printSomeAbracadabra (){
        System.out.println("test AutoCloseable, it's abracadabra");
    }

    @Override
    public void close() throws Exception {
        System.out.println("Close User Service class");
        throw new AutoCloseableCustomException("exception in AutoCloseable");

    }
}
