package com.hnatiievych.model;

public class AutoCloseableCustomException extends Exception {
    public AutoCloseableCustomException(String message) {
        super(message);
    }
}
