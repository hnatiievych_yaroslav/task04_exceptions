package com.hnatiievych.model;

public class UserAgeException extends RuntimeException{

    public UserAgeException(String message) {
        super(message);
    }
}
